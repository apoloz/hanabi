
import com.rits.cloning.Cloner;
import hanabi.hanabi_play.AbstractPlayer;
import hanabi.hanabi_play.Card;
import hanabi.hanabi_play.GameController;
import hanabi.hanabi_play.GameState;
import hanabi.hanabi_play.GameStateView;
import hanabi.hanabi_play.Hand;
import hanabi.hanabi_play.HumanStylePlayer;
import hanabi.hanabi_play.Move;
import hanabi.hanabi_play.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alex
 */
public final class MonteCarloTest {

    GameController controller;
    private Map<String, Integer> bestMove = new HashMap<>();
    private String firstMove;
    
    private boolean firstMoveBool;

    @Test
    public void testTwoMove() {
        Random random = new Random();
        random.setSeed(31756);
        GameState state = new GameState(false, 5, random);
        controller = new GameController(state, new Supplier<Player>() {
            @Override
            public Player get() {
                AbstractPlayer p = new HumanStylePlayer();
                p.setLoggingEnabled(true);
                return p;
            }
        }, true);

        int numPlayers = controller.getPlayers().length;
        for (int i = 0; i < numPlayers; ++i) {
            controller.getPlayers()[i].notifyGameStarted(new GameStateView(state, i), i);
        }
//        while (!state.isFinished()) {
        controller.runOneRound();
        bestMove = new HashMap<>();

        for (int m = 0; m < 100; m++) {
            firstMove = "0";
            firstMoveBool = true;
            monteCarlo(state, 2);
        }
        int mValue = 0;
        String mKey = null;
        for (Map.Entry<String, Integer> entry : bestMove.entrySet()) {
            if (mValue <= entry.getValue()) {
                mValue = entry.getValue();
                mKey = entry.getKey();
            }
        }

        System.out.println("decision tree 2 steps");
        System.out.println("best move: " + mKey + " ; max value = " + bestMove.get(mKey));
        assertTrue("decision tree 2 steps forward", true);
//        }
    }

    @Test
    public void testFiveMove() {
        Random random = new Random();
        random.setSeed(31756);
        GameState state = new GameState(false, 5, random);
        controller = new GameController(state, new Supplier<Player>() {
            @Override
            public Player get() {
                AbstractPlayer p = new HumanStylePlayer();
                p.setLoggingEnabled(true);
                return p;
            }
        }, true);

        int numPlayers = controller.getPlayers().length;
        for (int i = 0; i < numPlayers; ++i) {
            controller.getPlayers()[i].notifyGameStarted(new GameStateView(state, i), i);
        }
//        while (!state.isFinished()) {
        controller.runOneRound();
        bestMove = new HashMap<>();

        for (int m = 0; m < 100; m++) {
            firstMove = "0";
            firstMoveBool = true;
            monteCarlo(state, 5);
        }
        int mValue = 0;
        String mKey = null;
        for (Map.Entry<String, Integer> entry : bestMove.entrySet()) {
            if (mValue <= entry.getValue()) {
                mValue = entry.getValue();
                mKey = entry.getKey();
            }
        }

        System.out.println("decision tree 5 steps");
        System.out.println("best move: " + mKey + " ; max value = " + bestMove.get(mKey));
        assertTrue("decision tree 5 steps forward", true);//        }
    }

    @Test
    public void testEightMove() {
        Random random = new Random();
        random.setSeed(31756);
        GameState state = new GameState(false, 5, random);
        controller = new GameController(state, new Supplier<Player>() {
            @Override
            public Player get() {
                AbstractPlayer p = new HumanStylePlayer();
                p.setLoggingEnabled(true);
                return p;
            }
        }, true);

        int numPlayers = controller.getPlayers().length;
        for (int i = 0; i < numPlayers; ++i) {
            controller.getPlayers()[i].notifyGameStarted(new GameStateView(state, i), i);
        }
//        while (!state.isFinished()) {
        controller.runOneRound();
        bestMove = new HashMap<>();

        for (int m = 0; m < 100; m++) {
            firstMove = "0";
            firstMoveBool = true;
            monteCarlo(state, 8);
        }
        int mValue = 0;
        String mKey = null;
        for (Map.Entry<String, Integer> entry : bestMove.entrySet()) {
            if (mValue <= entry.getValue()) {
                mValue = entry.getValue();
                mKey = entry.getKey();
            }
        }

        System.out.println("decision tree 8 steps");
        System.out.println("best move: " + mKey + " ; max value = " + bestMove.get(mKey));
        assertTrue("decision tree 8 steps forward", true);
    }

    private String monteCarlo(GameState state, int depth) {
        int n = --depth;
        Cloner cloner = new Cloner();
        GameState cloneState = cloner.deepClone(state);
        if (n < 0) {
            boolean search = false;
            for (Map.Entry<String, Integer> entry : bestMove.entrySet()) {
                if (firstMove.equals(entry.getKey())) {
                    int plusOne = entry.getValue() + cloneState.getTableau();
                    bestMove.put(firstMove, plusOne);
                    search = true;
                }
            }
            if (!search) {
                bestMove.put(firstMove, 1);
            }
            return firstMove;
        }
        if (!cloneState.isFinished()) {
            Player[] clonePlayers = controller.getPlayers().clone();
            int move = 0;

            int player = cloneState.getCurrentPlayer();
            int hand = cloneState.getHand(player);
            int randMove;
            String info = "Player = " + (player + 1);
            if (cloneState.getHints() > 1) {
                randMove = (int) (Math.random() * 4);
            } else {
                randMove = (int) (Math.random() * 2);
            }
            int r = (int) (Math.random() * Hand.getSize(hand));
            switch (randMove) {

                case 0:
                    move = Move.discard(r);
                    info = info.concat(" discard № " + r);
                    break;
                case 1:
                    move = Move.play(r);
                    info = info.concat(" play № " + r);
                    break;
                case 2: {
                    boolean other = true;
                    while (other) {
                        int p = (int) (Math.random() * cloneState.getNumPlayers());
                        if (p != player) {
                            int handOther = cloneState.getHand(p);
                            int mmove = Hand.getCard(handOther, r);
                            move = Move.hintColor(p, Card.getColor(mmove));
                            other = false;
                            String color = "none";
                            switch (Card.getColor(mmove)) {
                                case 0:
                                    color = "white";
                                    break;
                                case 1:
                                    color = "blue";
                                    break;
                                case 2:
                                    color = "green";
                                    break;
                                case 3:
                                    color = "yellow";
                                    break;
                                case 4:
                                    color = "red";
                                    break;
                            }
                            info = info.concat(" hint Color " + color + " plaeyr " + (p + 1));
                        }
                    }
                    break;
                }
                case 3: {
                    boolean other = true;
                    while (other) {
                        int p = (int) (Math.random() * cloneState.getNumPlayers());
                        if (p != player) {
                            int handOther = cloneState.getHand(p);
                            int mmove = Hand.getCard(handOther, r);
                            move = Move.hintNumber(p, Card.getNumber(mmove));
                            other = false;
                            info = info.concat(" hint Number " + Card.getNumber(mmove) + " plaeyr " + (p + 1));
                        }
                    }
                    break;
                }
                default:
                    break;
            }

            int type = Move.getType(move);
            int removedCard;
            switch (type) {
                case Move.DISCARD:
                case Move.PLAY:
                    removedCard = Hand.getCard(hand, Move.getPosition(move));
                    break;
                case Move.HINT_COLOR:
                case Move.HINT_NUMBER:
                    removedCard = Card.NULL;
                    break;
                default:
                    throw new AssertionError();
            }
            int result = cloneState.applyMove(move);

            // We only really need to broadcast hints (the rest can be guessed from the game state), but broadcast the
            // whole log for convenience.
            switch (type) {
                case Move.DISCARD:
                    for (Player p : clonePlayers) {
                        p.notifyDiscard(removedCard, Move.getPosition(move), player);
                        if (result != Card.NULL) {
                            p.notifyDraw(p == clonePlayers[player] ? Card.NULL : result, player);
                        }
                    }
                    break;
                case Move.PLAY:
                    for (Player p : clonePlayers) {
                        p.notifyPlay(removedCard, Move.getPosition(move), player);
                        if (result != Card.NULL) {
                            p.notifyDraw(p == clonePlayers[player] ? Card.NULL : result, player);
                        }
                    }
                    break;
                case Move.HINT_COLOR: {
                    int color = Move.getHintContent(move);
                    int target = Move.getHintPlayer(move);
                    int match = Hand.matchCardsColor(cloneState.getHand(target), color);
                    for (Player p : clonePlayers) {
                        p.notifyHintColor(target, player, color, match);
                    }
                    break;
                }
                case Move.HINT_NUMBER: {
                    int number = Move.getHintContent(move);
                    int target = Move.getHintPlayer(move);
                    int match = Hand.matchCardsNumber(cloneState.getHand(target), number);
                    for (Player p : clonePlayers) {
                        p.notifyHintNumber(target, player, number, match);
                    }
                    break;
                }
                default:
                    throw new AssertionError();
            }

            if (firstMoveBool) {
//                System.out.println(info);
                bestMove.put(info, 0);
                firstMove = info;
                firstMoveBool = false;
            }
        } else {
            return firstMove;
        }
        return monteCarlo(cloneState, n);
    }
}

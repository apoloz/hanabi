package hanabi.hanabi_gui;

import hanabi.hanabi_play.Card;
import hanabi.hanabi_play.GameStateView;
import hanabi.hanabi_play.Hand;
import hanabi.hanabi_play.Move;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author scalan
 */
public class UserTurnDialog extends javax.swing.JDialog {

    private GameStateView state = null;
    private int me;
    private int move = Move.NULL;

    public final static int TURN_NONE = 0;
    public final static int TURN_ADVISE = 1;
    public final static int TURN_GIVEINFOBYCOLOR = 2;
    public final static int TURN_GIVEINFOBYNUMBER = 3;
    public final static int TURN_DISCARD = 4;
    public final static int TURN_PLAY = 5;

    int result = TURN_NONE;

    public static final int CARD_WIDTH = 56;
    public static final int CARD_HEIGHT = 87;
    private static final double SCALE = 0.3;
    private static final int BORDER = 1;

    private final JButton[][] giveInfoButtons;
    private final JLabel[] giveInfoCurrentPlayer;
    private final JLabel[] giveInfoPlayerLabel;

    private final JButton[][] discardButtons;
    private final JLabel[] discardCurrentPlayer;
    private final JLabel[] discardPlayerLabel;

    private final JButton[][] playButtons;
    private final JLabel[] playCurrentPlayer;
    private final JLabel[] playPlayerLabel;

    private class GiveInfoButtonAction extends AbstractAction {

        private int p, i;
        UserTurnDialog userTurnDialog = null;

        public GiveInfoButtonAction(int p, int i, UserTurnDialog userTurnDialog) {
            this.p = p;
            this.i = i;
            this.userTurnDialog = userTurnDialog;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            userTurnDialog.doGiveInfoChoiceCard(p, i);
        }
    }

    private class DiscardButtonAction extends AbstractAction {

        private int p, i;
        UserTurnDialog userTurnDialog = null;

        public DiscardButtonAction(int p, int i, UserTurnDialog userTurnDialog) {
            this.p = p;
            this.i = i;
            this.userTurnDialog = userTurnDialog;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            userTurnDialog.doDiscardChoiceCard(p, i);
        }
    }

    private class PlayButtonAction extends AbstractAction {

        private int p, i;
        UserTurnDialog userTurnDialog = null;

        public PlayButtonAction(int p, int i, UserTurnDialog userTurnDialog) {
            this.p = p;
            this.i = i;
            this.userTurnDialog = userTurnDialog;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            userTurnDialog.doPlayChoiceCard(p, i);
        }
    }

    /**
     * Creates new form UserTurnDialog
     */
    public UserTurnDialog(java.awt.Frame parent, boolean modal, GameStateView state, int me, int hintsColor[], int hintsNumber[]) {
        super(parent, modal);
        initComponents();

        this.me = me;

        jButtonAdvise.setVisible(false);
        jPanelButtons.remove(jButtonAdvise);
        
        // Карты соперников на панели поделиться информацией
        giveInfoButtons = new JButton[5][5];
        giveInfoCurrentPlayer = new JLabel[5];
        giveInfoPlayerLabel = new JLabel[5];
        for (int p = 0; p < 5; p++) {
            giveInfoCurrentPlayer[p] = new JLabel("*");
            giveInfoCurrentPlayer[p].setHorizontalAlignment(SwingConstants.CENTER);
            jPanelGiveInfoCards.add(giveInfoCurrentPlayer[p]);
            giveInfoPlayerLabel[p] = new JLabel(String.valueOf(p + 1));
            giveInfoPlayerLabel[p].setHorizontalAlignment(SwingConstants.CENTER);
            jPanelGiveInfoCards.add(giveInfoPlayerLabel[p]);

            for (int i = 0; i < 5; i++) {
                JButton l = new JButton(String.valueOf(i + 1));
                l.setAction(new GiveInfoButtonAction(p, i, this));
                giveInfoButtons[p][i] = l;
                l.setPreferredSize(new Dimension((int) (CARD_WIDTH * SCALE), (int) (CARD_HEIGHT * SCALE)));
                l.setBackground(TableauUI.COLORS[p]);
                l.setOpaque(true);
                l.setHorizontalAlignment(JLabel.CENTER);
                l.setBorder(BorderFactory.createMatteBorder(BORDER, BORDER, BORDER, BORDER, Color.BLACK));
                jPanelGiveInfoCards.add(l);
            }
        }

        // Свои карты на панели сбросить карту
        discardButtons = new JButton[5][5];
        discardCurrentPlayer = new JLabel[5];
        discardPlayerLabel = new JLabel[5];
        for (int p = 0; p < 5; p++) {
            if (p == me) {
                discardCurrentPlayer[p] = new JLabel("*");
                discardCurrentPlayer[p].setHorizontalAlignment(SwingConstants.CENTER);
                jPanelDiscardCards.add(discardCurrentPlayer[p]);
                discardPlayerLabel[p] = new JLabel(String.valueOf(p + 1));
                discardPlayerLabel[p].setHorizontalAlignment(SwingConstants.CENTER);
                jPanelDiscardCards.add(discardPlayerLabel[p]);

                for (int i = 0; i < 5; i++) {
                    JButton l = new JButton(String.valueOf(i + 1));
                    l.setAction(new DiscardButtonAction(p, i, this));
                    discardButtons[p][i] = l;
                    l.setPreferredSize(new Dimension((int) (CARD_WIDTH * SCALE), (int) (CARD_HEIGHT * SCALE)));

                    l.setBackground(TableauUI.COLORS[p]);
                    l.setOpaque(true);
                    l.setHorizontalAlignment(JLabel.CENTER);
                    l.setBorder(BorderFactory.createMatteBorder(BORDER, BORDER, BORDER, BORDER, Color.BLACK));
                    jPanelDiscardCards.add(l);
                }
            }
        }

        // Свои карты на панели разыграть карту
        playButtons = new JButton[5][5];
        playCurrentPlayer = new JLabel[5];
        playPlayerLabel = new JLabel[5];
        for (int p = 0; p < 5; p++) {
            if (p == me) {
                playCurrentPlayer[p] = new JLabel("*");
                playCurrentPlayer[p].setHorizontalAlignment(SwingConstants.CENTER);
                jPanelPlayCards.add(playCurrentPlayer[p]);
                playPlayerLabel[p] = new JLabel(String.valueOf(p + 1));
                playPlayerLabel[p].setHorizontalAlignment(SwingConstants.CENTER);
                jPanelPlayCards.add(playPlayerLabel[p]);

                for (int i = 0; i < 5; i++) {
                    JButton l = new JButton(String.valueOf(i + 1));
                    l.setAction(new PlayButtonAction(p, i, this));
                    playButtons[p][i] = l;
                    l.setPreferredSize(new Dimension((int) (CARD_WIDTH * SCALE), (int) (CARD_HEIGHT * SCALE)));
                    l.setBackground(TableauUI.COLORS[p]);
                    l.setOpaque(true);
                    l.setHorizontalAlignment(JLabel.CENTER);
                    l.setBorder(BorderFactory.createMatteBorder(BORDER, BORDER, BORDER, BORDER, Color.BLACK));
                    jPanelPlayCards.add(l);
                }
            }
        }

        jPanelAdvise.setVisible(false);
        jPanelGiveInfo.setVisible(true);
        jPanelDiscard.setVisible(false);
        jPanelPlay.setVisible(false);
        this.state = state;
        if (state != null) {
            if (state.getHints() > 0) {
                // Доступна кнопка поделиться информацией
                this.jButtonGiveInfo.setEnabled(true);
                // Вывести карты соперников на панели поделиться информацией
                for (int p = 0; p < state.getNumPlayers(); p++) {
                    giveInfoCurrentPlayer[p].setVisible(state.getCurrentPlayer() == p);
                    giveInfoPlayerLabel[p].setVisible(true);
                    int hand = state.getHandUnsafe(p);
                    for (int i = 0; i < 5; i++) {
                        if (i < Hand.getSize(hand)) {
                            int card = Hand.getCard(hand, i);
                            if (p == me) {
                                giveInfoButtons[p][i].setText(" ");
                                int hintNumber = hintsNumber[p];
                                if ((hintNumber & (1 << i)) != 0) {
                                    giveInfoButtons[p][i].setText(String.valueOf(Card.getNumber(card) + 1));
                                }
                                giveInfoButtons[p][i].setBackground(Color.DARK_GRAY);
                                int hintColor = hintsColor[p];
                                if ((hintColor & (1 << i)) != 0) {
                                    giveInfoButtons[p][i].setBackground(TableauUI.COLORS[Card.getColor(card)]);
                                }
                            } else {
                                giveInfoButtons[p][i].setText(String.valueOf(Card.getNumber(card) + 1));
                                giveInfoButtons[p][i].setBackground(TableauUI.COLORS[Card.getColor(card)]);
                            }
                            giveInfoButtons[p][i].setVisible(true);
                        } else {
                            giveInfoButtons[p][i].setVisible(false);
                        }
                        if (p == me) {
                            giveInfoButtons[p][i].setEnabled(false);
                        }
                    }
                }
                for (int p = state.getNumPlayers(); p < 5; p++) {
                    giveInfoCurrentPlayer[p].setVisible(false);
                    giveInfoPlayerLabel[p].setVisible(false);
                    for (int i = 0; i < 5; i++) {
                        giveInfoButtons[p][i].setVisible(false);
                    }
                }
            } else {
                // Недоступна кнопка поделиться информацией
                this.jButtonGiveInfo.setEnabled(false);
            }

            // Вывести карты соперников на панели сбросить карту
            for (int p = 0; p < state.getNumPlayers(); p++) {
                if (p == me) {
                    discardCurrentPlayer[p].setVisible(state.getCurrentPlayer() == p);
                    discardPlayerLabel[p].setVisible(true);
                    int hand = state.getHandUnsafe(p);
                    for (int i = 0; i < 5; i++) {
                        if (i < Hand.getSize(hand)) {
                            discardButtons[p][i].setText(" ");
                            int card = Hand.getCard(hand, i);
                            int hintNumber = hintsNumber[p];
                            if ((hintNumber & (1 << i)) != 0) {
                                discardButtons[p][i].setText(String.valueOf(Card.getNumber(card) + 1));
                            }
                            discardButtons[p][i].setBackground(Color.DARK_GRAY);
                            int hintColor = hintsColor[p];
                            if ((hintColor & (1 << i)) != 0) {
                                discardButtons[p][i].setBackground(TableauUI.COLORS[Card.getColor(card)]);
                            }
                            discardButtons[p][i].setVisible(true);
                        } else {
                            discardButtons[p][i].setVisible(false);
                        }
                    }
                }
            }

            // Вывести карты соперников на панели разыграть карту
            for (int p = 0; p < state.getNumPlayers(); p++) {
                if (p == me) {
                    playCurrentPlayer[p].setVisible(state.getCurrentPlayer() == p);
                    playPlayerLabel[p].setVisible(true);
                    int hand = state.getHandUnsafe(p);
                    for (int i = 0; i < 5; i++) {
                        if (i < Hand.getSize(hand)) {
                            playButtons[p][i].setText(" ");
                            int card = Hand.getCard(hand, i);
                            int hintNumber = hintsNumber[p];
                            if ((hintNumber & (1 << i)) != 0) {
                                playButtons[p][i].setText(String.valueOf(Card.getNumber(card) + 1));
                            }
                            playButtons[p][i].setBackground(Color.DARK_GRAY);
                            int hintColor = hintsColor[p];
                            if ((hintColor & (1 << i)) != 0) {
                                playButtons[p][i].setBackground(TableauUI.COLORS[Card.getColor(card)]);
                            }
                            playButtons[p][i].setVisible(true);
                        } else {
                            playButtons[p][i].setVisible(false);
                        }
                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelButtons = new javax.swing.JPanel();
        jButtonAdvise = new javax.swing.JButton();
        jButtonGiveInfo = new javax.swing.JButton();
        jButtonDiscard = new javax.swing.JButton();
        jButtonPlay = new javax.swing.JButton();
        jPanelInfo = new javax.swing.JPanel();
        jPanelAdvise = new javax.swing.JPanel();
        jLabelAdviseText = new javax.swing.JLabel();
        jPanelGiveInfo = new javax.swing.JPanel();
        jPanelGiveInfoCards = new javax.swing.JPanel();
        jPanelGiveInfoActions = new javax.swing.JPanel();
        jLabelGiveInfoCard = new javax.swing.JLabel();
        jButtonGiveInfoByColor = new javax.swing.JButton();
        jButtonGiveInfoByNumber = new javax.swing.JButton();
        jPanelDiscard = new javax.swing.JPanel();
        jPanelDiscardCards = new javax.swing.JPanel();
        jPanelDiscardActions = new javax.swing.JPanel();
        jLabelDiscardCard = new javax.swing.JLabel();
        jButtonDiscardAction = new javax.swing.JButton();
        jPanelPlay = new javax.swing.JPanel();
        jPanelPlayCards = new javax.swing.JPanel();
        jPanelPlayActions = new javax.swing.JPanel();
        jLabelPlayCard = new javax.swing.JLabel();
        jButtonPlayAction = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Ваш ход.");
        setAlwaysOnTop(true);

        jPanelButtons.setLayout(new java.awt.GridLayout(1, 0));

        jButtonAdvise.setText("Advise");
        jButtonAdvise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdviseActionPerformed(evt);
            }
        });
        jPanelButtons.add(jButtonAdvise);

        jButtonGiveInfo.setText("Поделиться инфо");
        jButtonGiveInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGiveInfoActionPerformed(evt);
            }
        });
        jPanelButtons.add(jButtonGiveInfo);

        jButtonDiscard.setText("Сбросить карту");
        jButtonDiscard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDiscardActionPerformed(evt);
            }
        });
        jPanelButtons.add(jButtonDiscard);

        jButtonPlay.setText("Разыграть карту");
        jButtonPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPlayActionPerformed(evt);
            }
        });
        jPanelButtons.add(jButtonPlay);

        getContentPane().add(jPanelButtons, java.awt.BorderLayout.NORTH);

        jPanelInfo.setLayout(new java.awt.GridBagLayout());

        jPanelAdvise.setLayout(new java.awt.BorderLayout());

        jLabelAdviseText.setText("Получить совет от компьютера и сделать ход.");
        jPanelAdvise.add(jLabelAdviseText, java.awt.BorderLayout.CENTER);

        jPanelInfo.add(jPanelAdvise, new java.awt.GridBagConstraints());

        jPanelGiveInfo.setLayout(new java.awt.BorderLayout());

        jPanelGiveInfoCards.setLayout(new java.awt.GridLayout(5, 5, 2, 3));
        jPanelGiveInfo.add(jPanelGiveInfoCards, java.awt.BorderLayout.CENTER);

        jLabelGiveInfoCard.setText(" ");
        jLabelGiveInfoCard.setOpaque(true);
        jPanelGiveInfoActions.add(jLabelGiveInfoCard);

        jButtonGiveInfoByColor.setText("По цвету");
        jButtonGiveInfoByColor.setEnabled(false);
        jButtonGiveInfoByColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGiveInfoByColorActionPerformed(evt);
            }
        });
        jPanelGiveInfoActions.add(jButtonGiveInfoByColor);

        jButtonGiveInfoByNumber.setText("По номеру");
        jButtonGiveInfoByNumber.setEnabled(false);
        jButtonGiveInfoByNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGiveInfoByNumberActionPerformed(evt);
            }
        });
        jPanelGiveInfoActions.add(jButtonGiveInfoByNumber);

        jPanelGiveInfo.add(jPanelGiveInfoActions, java.awt.BorderLayout.SOUTH);

        jPanelInfo.add(jPanelGiveInfo, new java.awt.GridBagConstraints());

        jPanelDiscard.setLayout(new java.awt.BorderLayout());

        jPanelDiscardCards.setLayout(new java.awt.GridLayout(1, 5, 2, 3));
        jPanelDiscard.add(jPanelDiscardCards, java.awt.BorderLayout.CENTER);

        jLabelDiscardCard.setText(" ");
        jLabelDiscardCard.setOpaque(true);
        jPanelDiscardActions.add(jLabelDiscardCard);

        jButtonDiscardAction.setText("Сбросить");
        jButtonDiscardAction.setEnabled(false);
        jButtonDiscardAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDiscardActionActionPerformed(evt);
            }
        });
        jPanelDiscardActions.add(jButtonDiscardAction);

        jPanelDiscard.add(jPanelDiscardActions, java.awt.BorderLayout.SOUTH);

        jPanelInfo.add(jPanelDiscard, new java.awt.GridBagConstraints());

        jPanelPlay.setLayout(new java.awt.BorderLayout());

        jPanelPlayCards.setLayout(new java.awt.GridLayout(1, 5, 2, 3));
        jPanelPlay.add(jPanelPlayCards, java.awt.BorderLayout.CENTER);

        jLabelPlayCard.setText(" ");
        jLabelPlayCard.setOpaque(true);
        jPanelPlayActions.add(jLabelPlayCard);

        jButtonPlayAction.setText("Разыграть");
        jButtonPlayAction.setEnabled(false);
        jButtonPlayAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPlayActionActionPerformed(evt);
            }
        });
        jPanelPlayActions.add(jButtonPlayAction);

        jPanelPlay.add(jPanelPlayActions, java.awt.BorderLayout.SOUTH);

        jPanelInfo.add(jPanelPlay, new java.awt.GridBagConstraints());

        getContentPane().add(jPanelInfo, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(525, 273));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAdviseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdviseActionPerformed
        jPanelAdvise.setVisible(true);
        jPanelGiveInfo.setVisible(false);
        jPanelDiscard.setVisible(false);
        jPanelPlay.setVisible(false);
        result = TURN_ADVISE;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButtonAdviseActionPerformed

    private void jButtonGiveInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGiveInfoActionPerformed
        jPanelAdvise.setVisible(false);
        jPanelGiveInfo.setVisible(true);
        jPanelDiscard.setVisible(false);
        jPanelPlay.setVisible(false);
    }//GEN-LAST:event_jButtonGiveInfoActionPerformed

    private void jButtonDiscardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDiscardActionPerformed
        jPanelAdvise.setVisible(false);
        jPanelGiveInfo.setVisible(false);
        jPanelDiscard.setVisible(true);
        jPanelPlay.setVisible(false);
    }//GEN-LAST:event_jButtonDiscardActionPerformed

    private void jButtonGiveInfoByColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGiveInfoByColorActionPerformed
        move = Move.hintColor(giveInfoCardP, giveInfoCardColor);
        result = TURN_GIVEINFOBYCOLOR;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButtonGiveInfoByColorActionPerformed

    private void jButtonGiveInfoByNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGiveInfoByNumberActionPerformed
        move = Move.hintNumber(giveInfoCardP, giveInfoCardNumber);
        result = TURN_GIVEINFOBYNUMBER;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButtonGiveInfoByNumberActionPerformed

    private void jButtonDiscardActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDiscardActionActionPerformed
        move = Move.discard(discardCardN);
        result = TURN_DISCARD;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButtonDiscardActionActionPerformed

    private void jButtonPlayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPlayActionPerformed
        jPanelAdvise.setVisible(false);
        jPanelGiveInfo.setVisible(false);
        jPanelDiscard.setVisible(false);
        jPanelPlay.setVisible(true);
    }//GEN-LAST:event_jButtonPlayActionPerformed

    private void jButtonPlayActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPlayActionActionPerformed
        move = Move.play(playCardN);
        result = TURN_PLAY;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButtonPlayActionActionPerformed

    int giveInfoCardP = 0;
    int giveInfoCardColor = 0;
    int giveInfoCardNumber = 0;

    public void doGiveInfoChoiceCard(int p, int i) {
        int hand = state.getHandUnsafe(p);
        int card = Hand.getCard(hand, i);
        jLabelGiveInfoCard.setText(String.valueOf(Card.getNumber(card) + 1));
        jLabelGiveInfoCard.setBackground(TableauUI.COLORS[Card.getColor(card)]);
        jLabelGiveInfoCard.setBorder(BorderFactory.createMatteBorder(BORDER, BORDER, BORDER, BORDER, Color.BLACK));
        jLabelGiveInfoCard.setHorizontalAlignment(JLabel.CENTER);
        jButtonGiveInfoByColor.setEnabled(true);
        jButtonGiveInfoByNumber.setEnabled(true);
        giveInfoCardP = p;
        giveInfoCardColor = Card.getColor(card);
        giveInfoCardNumber = Card.getNumber(card);
    }

    int discardCardP = 0;
    int discardCardN = 0;

    public void doDiscardChoiceCard(int p, int i) {
        int hand = state.getHandUnsafe(p);
        int card = Hand.getCard(hand, i);
        jButtonDiscardAction.setEnabled(true);
        discardCardP = p;
        discardCardN = i;
    }

    int playCardP = 0;
    int playCardN = 0;

    public void doPlayChoiceCard(int p, int i) {
        int hand = state.getHandUnsafe(p);
        int card = Hand.getCard(hand, i);
        jButtonPlayAction.setEnabled(true);
        playCardP = p;
        playCardN = i;
    }

    public int showDialog() {
        setVisible(true);
        return result;
    }

    /**
     * @return the move
     */
    public int getMove() {
        return move;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdvise;
    private javax.swing.JButton jButtonDiscard;
    private javax.swing.JButton jButtonDiscardAction;
    private javax.swing.JButton jButtonGiveInfo;
    private javax.swing.JButton jButtonGiveInfoByColor;
    private javax.swing.JButton jButtonGiveInfoByNumber;
    private javax.swing.JButton jButtonPlay;
    private javax.swing.JButton jButtonPlayAction;
    private javax.swing.JLabel jLabelAdviseText;
    private javax.swing.JLabel jLabelDiscardCard;
    private javax.swing.JLabel jLabelGiveInfoCard;
    private javax.swing.JLabel jLabelPlayCard;
    private javax.swing.JPanel jPanelAdvise;
    private javax.swing.JPanel jPanelButtons;
    private javax.swing.JPanel jPanelDiscard;
    private javax.swing.JPanel jPanelDiscardActions;
    private javax.swing.JPanel jPanelDiscardCards;
    private javax.swing.JPanel jPanelGiveInfo;
    private javax.swing.JPanel jPanelGiveInfoActions;
    private javax.swing.JPanel jPanelGiveInfoCards;
    private javax.swing.JPanel jPanelInfo;
    private javax.swing.JPanel jPanelPlay;
    private javax.swing.JPanel jPanelPlayActions;
    private javax.swing.JPanel jPanelPlayCards;
    // End of variables declaration//GEN-END:variables
}

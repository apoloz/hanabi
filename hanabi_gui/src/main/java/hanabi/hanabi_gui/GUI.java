package hanabi.hanabi_gui;

import hanabi.hanabi_play.AbstractPlayer;
import hanabi.hanabi_play.GameController;
import hanabi.hanabi_play.GameState;
import hanabi.hanabi_play.HumanStylePlayer;
import hanabi.hanabi_play.Player;
import hanabi.hanabi_play.RandomUtil;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI {
    static GameState state;

    private static final int MOVE_INTERVAL = 500;
    private static final int GAME_INTERVAL = 3000;
    static boolean st = false;

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        final HanabiUI hanabi = new HanabiUI();

        final JPanel pnlButton = new JPanel();
        JPanel nordPanel = new JPanel(new FlowLayout());

        JLabel textCount = new JLabel("Количество игроков");
        nordPanel.add(textCount);
        pnlButton.setLayout(new BorderLayout());
        Integer[] playerItems = {
            3,
            4,
            5
        };
        JComboBox comboBox = new JComboBox(playerItems);
        nordPanel.add(comboBox);
        JButton startButton = new JButton("Start");
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                st = true;
            }
        });
        nordPanel.add(startButton);
        pnlButton.add("North", nordPanel);
        pnlButton.add("South", hanabi);

        final JFrame frame = new JFrame("Hanabi bot");
        SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setContentPane(pnlButton);
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });

        while (true) {
            if (st) {
                state = new GameState(false, (int) comboBox.getSelectedItem(), RandomUtil.INSTANCE);
                GameController controller = new GameController(state, new Supplier<Player>() {
                    int i = 0;

                    @Override
                    public Player get() {
                        AbstractPlayer p;
                        if (i == 0) {
                            p = new RealPlayer(frame);
                        } else {
                            p = new HumanStylePlayer();
                        }
                        p.setLoggingEnabled(true);
                        i++;
                        return p;

                    }
                }, true);

                controller.setTurnHook(
                        new Consumer<Integer>() {
                    @Override
                    public void accept(Integer move
                    ) {
                        try {
                            try {
                                SwingUtilities.invokeAndWait(new Runnable() {
                                    @Override
                                    public void run() {
                                        hanabi.update(state);
                                    }
                                });
                                Thread.sleep(MOVE_INTERVAL);
                            } catch (InvocationTargetException ex) {
                                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (InterruptedException ex) {
                            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                );
                SwingUtilities.invokeAndWait(
                        new Runnable() {
                    @Override
                    public void run() {
                        hanabi.update(state);
                    }
                }
                );
                controller.run();
                st = false;
            } else {
                if (state != null) {
                    state = null;
                }

                Thread.sleep(1000);
                System.out.println("Thread.sleep(false);" + st);
            }
        }
    }
}
